#include "pretoebranco.hpp"

using namespace std;

PretoeBranco::PretoeBranco(){

	setLargura(0);
	setAltura(0);
	setEscalaMaxima(0);

}

PretoeBranco::~PretoeBranco(){

}

PretoeBranco::PretoeBranco(int largura, int altura, int escala_maxima){
	setLargura(largura);
	setAltura(altura);
	setEscalaMaxima(escala_maxima);

}

void PretoeBranco::gravaPixel(ofstream &novaImagem){

	int largura, altura, escala_maxima;
	char grey;

	largura = getLargura();
	altura = getAltura();
	escala_maxima = getEscalaMaxima();

	for(int i = 0; i < altura; i++)
	{
		for(int j = 0; j < largura; j++)
		{
		grey = (0.299*red[i][j]) + (0.587*green[i][j]) + (0.144*blue[i][j]);
		if(grey > escala_maxima)
		{
		red[i][j] = escala_maxima;
		green[i][j] = escala_maxima;
		blue[i][j] = escala_maxima;
		novaImagem << red[i][j] << green[i][j] << blue[i][j];
		}
		else
		{
		red[i][j] = grey;
		green[i][j] = grey;
		blue[i][j] = grey;
		novaImagem << red[i][j] << green[i][j] << blue[i][j];
		}

		}

	}

		cout << "\n Filtro aplicado com sucesso!" << endl;
		cout << "\n A nova imagem foi salva na pasta Doc" << endl;

}
