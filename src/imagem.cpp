#include "imagem.hpp"
using namespace std;

Imagem:: Imagem(){

	numero_magico = " ";
	altura = 0;
	largura = 0;
	escala_maxima = 255;
}

Imagem::~Imagem(){

}

Imagem::Imagem(string numero_magico, int largura, int altura, int escala_maxima){
	this->numero_magico = numero_magico;
	this->largura = largura;
	this->altura = altura;
	this->escala_maxima = escala_maxima;
}

string Imagem::getNumeroMagico(){
	return numero_magico;
}

void Imagem::setNumeroMagico(string numero_magico){
	this->numero_magico = numero_magico;
}

int Imagem::getLargura(){
	return largura;
}

void Imagem::setLargura(int largura){
	this->largura = largura;
}

int Imagem::getAltura(){
	return altura;
}

void Imagem::setAltura(int altura){
	this->altura = altura;
}

int Imagem::getEscalaMaxima(){
	return escala_maxima;
}

void Imagem::setEscalaMaxima(int escala_maxima){
	this->escala_maxima = escala_maxima;
}

