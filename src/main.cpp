#include "imagem.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "pretoebranco.hpp"
#include "manipula.hpp"

char opcao;

int main (){

	ofstream novaImagem;
	
	//Manipula *manipula = new Manipula();
	Negativo *negativo = new Negativo();
	Polarizado *polarizado = new Polarizado();
	PretoeBranco *pretoebranco = new PretoeBranco();

	while(opcao!='4'){
	    cout << "\n--------------------------------------" << endl;
            cout << "Escolha um filtro para ser aplicado na imagem:" << endl;
            cout << " 1 - Negativo" << endl;
            cout << " 2 - Polarizado" << endl;
	    cout << " 3 - Preto e Branco" << endl;
	    cout << " 4 - Sair" << endl;
            cout << "----------------------------------------\n" << endl;;
         cin >> opcao;

        switch (opcao)
        {
            case '1':
            {
		negativo->aplicaFiltro(novaImagem);
		break;
	    }
	    case '2':
	    { 
		polarizado->aplicaFiltro(novaImagem);
		break;
	    }
	    case '3':
	    { 
		pretoebranco->aplicaFiltro(novaImagem);
		break; 
	    }
            case '4':
	    {
		cout << "Saindo..." << endl;
		break;

		default:
		 cout << " Opcao invalida tente novamente" << endl;
		break;
	    }
	}
   }	
	return 0;

}
