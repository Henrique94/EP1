#include "polarizado.hpp"

using namespace std;

Polarizado::Polarizado(){

	setLargura(0);
	setAltura(0);
	setEscalaMaxima(0);

}

Polarizado::~Polarizado(){

}

Polarizado::Polarizado(int largura, int altura, int escala_maxima){
	setLargura(largura);
	setAltura(altura);
	setEscalaMaxima(escala_maxima);

}

void Polarizado::gravaPixel(ofstream &novaImagem){

	int largura, altura, escala_maxima;
	unsigned int vermelho, verde, azul;

	largura = getLargura();
	altura = getAltura();
	escala_maxima = getEscalaMaxima();

	for(int i = 0; i < altura; i++)
	{
		for(int j = 0; j < largura; j++)
		{
		vermelho = red[i][j];
		if(vermelho > (unsigned int)escala_maxima/2)
		red[i][j] = escala_maxima;
		else
		red[i][j] = 0;

		verde = green[i][j];
		if(verde > (unsigned int)escala_maxima/2)
		green[i][j] = escala_maxima;
		else
		green[i][j] = 0;

		azul = blue[i][j];
		if(azul > (unsigned int)escala_maxima/2)
		blue[i][j] = escala_maxima;
		else
		blue[i][j] = 0;

		novaImagem << red[i][j];
		novaImagem << green[i][j];
		novaImagem << blue[i][j];


		}

	}
		cout << "\n Filtro aplicado com sucesso!" << endl;
		cout << "\n A nova imagem foi salva na pasta Doc" << endl;

}
