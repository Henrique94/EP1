#include "manipula.hpp"
#include <cmath>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

Manipula:: Manipula(){

	int largura, altura;

	largura = getLargura();
	altura = getAltura();

	red = new char*[altura];
	for(int i = 0; i < altura; i++)
	red[i] = new char[largura];

	green = new char*[altura];
	for(int i = 0; i < altura; i++)
	green[i] = new char[largura];

	blue = new char*[altura];
	for(int i = 0; i < altura; i++)
	blue[i] = new char[largura];

}

Manipula::~Manipula(){
}

void Manipula::aplicaFiltro(ofstream &novaImagem){

	ifstream imagem;
	string imagem_original;

	string numero_magico, comentario;
	int largura, altura, escala_maxima, coment_tamanho;

	int j, k;
	char r, g, b;

	cout << "Digite apenas o nome da imagem contida na pasta Doc (por exemplo: unb): " << endl;;
	cin >> imagem_original;

	imagem_original = "./doc/" + imagem_original + ".ppm";

	imagem.open(imagem_original.c_str(), ios::binary);

	if(!imagem.is_open())
	{

	cout << "A imagem nao foi encontrada, certifique-se que ela esta na pasta Doc!" << endl;
	return;

	}
	else
	{
	getline(imagem,numero_magico);

	if(numero_magico != "P6")
	{
	cout << "Este programa ler apenas imagem ppm, verifique o tipo de imagem que deseja aplicar o filtro!" << endl;
	return;
	}

	while(1)
	{

	getline(imagem,comentario);

	if(comentario[0] != '#')
	{
	coment_tamanho = comentario.length()+1;
	imagem.seekg(-coment_tamanho,ios_base::cur);
	imagem >> largura >> altura >> escala_maxima;
	break;
	}
   }

	setNumeroMagico(numero_magico);
	setLargura(largura);
	setAltura(altura);
	setEscalaMaxima(escala_maxima);

	imagem.seekg(1,ios_base::cur);

	string imagem_modificada;

	cout << "Digite o nome da nova imagem: " << endl;
	cin >> imagem_modificada;
	imagem_modificada = "./doc/" + imagem_modificada + ".ppm";

	while(imagem_modificada == imagem_original)
	{
	cout << "Nome de arquivo. Por favor, insira um novo nome diferente da imagem original: ";
	cin >> imagem_modificada;
	}

	novaImagem.open(imagem_modificada.c_str(),ios::binary);

	if(!novaImagem.is_open())
	{
	cout << "Opa! Ocorreu um erro ao tentar criar a nova imagem!" << endl;
	return;
	}

	novaImagem << numero_magico << endl;
	novaImagem << largura << " " << altura << endl;
	novaImagem << escala_maxima << endl;

	red = new char*[altura];
	for(int i = 0; i < altura; i++)
	red[i] = new char[largura];

	green = new char*[altura];
	for(int i = 0; i < altura; i++)
	green[i] = new char[largura];

	blue = new char*[altura];
	for(int i = 0; i < altura; i++)
	blue[i] = new char[largura];


	for(j = 0; j < altura; j++)
	{
		for(k = 0; k < largura; k++)
		{
		imagem.get(r);
		imagem.get(g);
		imagem.get(b);
		red[j][k] = r;
		green[j][k] = g;
		blue[j][k] = b;
		}

	}

	}

	gravaPixel(novaImagem);

	imagem.close();
	novaImagem.close();

}

void Manipula::gravaPixel(ofstream &novaImagem){

int largura, altura;

	largura = getLargura();
	altura = getAltura();


	int **vermelho, **verde, **azul;

	vermelho = new int*[altura];
	for(int i = 0; i < altura; i++)
	vermelho[i] = new int[largura];

	verde = new int*[altura];
	for(int i = 0; i < altura; i++)
	verde[i] = new int[largura];

	azul = new int*[altura];
	for(int i = 0; i < altura; i++)
	azul[i] = new int[largura];


	int i, j;

	for(i = 0; i < altura; i++)
	{
		for(j = 0; j < largura; j++)
		{
		vermelho[i][j] = (unsigned int)red[i][j];
		novaImagem << (char)vermelho[i][j];
		verde[i][j] = (unsigned int)green[i][j];
		novaImagem << (char)verde[i][j];
		azul[i][j] = (unsigned int)blue[i][j];
		novaImagem << (char)azul[i][j];
		}
	}


}

