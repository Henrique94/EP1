#include "negativo.hpp"
#include <cstdio>
#include <cstdlib>

using namespace std;

Negativo::Negativo(){

	setLargura(0);
	setAltura(0);
	setEscalaMaxima(0);
}

Negativo::~Negativo(){

}

Negativo::Negativo(int largura, int altura, int escala_maxima){
	setLargura(largura);
	setAltura(altura);
	setEscalaMaxima(escala_maxima);
}

void Negativo::gravaPixel(ofstream &novaImagem){

	int largura, altura, escala_maxima;

	largura = getLargura();
	altura = getAltura();
	escala_maxima = getEscalaMaxima();

	for(int i = 0; i < altura; i++)
	{
		for(int j = 0; j < largura; j++)
		{
		red[i][j] = escala_maxima - red[i][j];
		novaImagem << red[i][j];

		green[i][j] = escala_maxima - green[i][j];
		novaImagem << green[i][j];

		blue[i][j] = escala_maxima - blue[i][j];
		novaImagem << blue[i][j];
		}
	}
		cout << "\n Filtro aplicado com sucesso!" << endl;
		cout << "\n A nova imagem foi salva na pasta Doc" << endl;

}
