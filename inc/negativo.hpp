#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include <iostream>
#include "imagem.hpp"
#include "manipula.hpp"

class Negativo : public Manipula {

public:

	Negativo();
	~Negativo();
	Negativo(int largura, int altura, int escala_maxima);
	void gravaPixel(ofstream &novaImagem);

};

#endif
