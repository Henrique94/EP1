#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP

#include <iostream>
#include "imagem.hpp"
#include "manipula.hpp"
class Polarizado : public Manipula{

public:

	Polarizado();
	~Polarizado();
	Polarizado(int largura, int altura, int escala_maxima);
	void gravaPixel(ofstream &novaImagem);




};

#endif
