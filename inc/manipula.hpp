#ifndef MANIPULA_HPP
#define MANIPULA_HPP

#include "imagem.hpp"

using namespace std;

class Manipula : public Imagem {
	public:
		char **red, **green, **blue;
		ofstream novaImagem;

		Manipula();
		~Manipula();

		void aplicaFiltro(ofstream &novaImagem);
		virtual	void gravaPixel(ofstream &novaImagem);

};

#endif 
