#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;
class Imagem {
	private:
		string numero_magico, comentario;
		int largura, altura, escala_maxima;
		
	public:
		Imagem();
		~Imagem();

		Imagem(string numero_magico,int largura, int altura, int escala_maxima);

		string getNumeroMagico();
		void setNumeroMagico(string numero_magico);

		string getComentario();
		void setComentario(string comentario);

		int getLargura();
		void setLargura(int largura);

		int getAltura();
		void setAltura(int altura);

		int getEscalaMaxima();
		void setEscalaMaxima(int escala_maxima);
	
};
#endif
