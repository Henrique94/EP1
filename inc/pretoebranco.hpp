#ifndef PRETOEBRANCO_HPP
#define PRETOEBRANCO_HPP

#include <iostream>
#include "imagem.hpp"
#include "manipula.hpp"

class PretoeBranco : public Manipula {

public:

	PretoeBranco();
	~PretoeBranco();
	PretoeBranco(int largura, int altura, int escala_maxima);
	void gravaPixel(ofstream &novaImagem);

};

#endif
