Link do projeto no gitLab: https://gitlab.com/Henrique94/EP1

bin: Diretório onde contem o binário.

include: Diretório que contem os headers.

source: Diretório com a implementação dos headers.

obj: Diretório que recebe os objetos após compilado.

doc: Diretório contendo as imagens a serem utilizadas neste trabalho.


Neste projeto foi implementaod o arquivo Makefile.

Para compilar o programa abra o terminal no diretório EP1 e digite: make
Para execultar o programa, basta digitar: make run  
Caso queira remover os objetos e o binario dos diretorio digite: make clean

Instruções:
O programa ira solicitar o tipo de filtro que se deseja aplicar a imagem em um simples menu, basta digitar o número correspondente
ao filtro desejado, ou a opçao 4 para sair do menu.
Feito isso, será solicitado uma imagem, basta digitar apenas o nome de alguma imagem contida na pasta doc, por exemplo, digite: unb
Caso não ocorra nenhum erro na abertura da imagem o programa ira pedir um novo nome para a nova imagem, digite apenas o nome, sem extensão,
por exemplo: unb2
Feito isso, a imagem será salva na pasta Doc, e novamente irá apresentar um novo menu para o usuário, até que o mesmo o encerre escolhendo
a opção 4.

